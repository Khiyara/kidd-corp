from django.urls import path
from .views import index, contact, regis, jadwal, agenda, reset

urlpatterns = [
    path('', index, name='index'),
	path('contact/', contact, name='contact'),
	path('regis/', regis, name='regis'),
	path('jadwal/', jadwal, name='jadwal'),
	path('agenda/', agenda, name='agenda'),
	path('agenda/reset', reset, name='reset')
]