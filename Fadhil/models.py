from django.db import models

# Create your models here.
class Jadwal(models.Model):
	name = models.CharField(max_length=100)
	location = models.CharField(max_length=30)
	category = models.CharField(max_length=30)
	day = models.CharField(max_length=10)
	date = models.DateField()
	time = models.TimeField()