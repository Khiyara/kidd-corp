from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import *
from .models import *

def index(request):
	return render(request, 'tes.html')

def contact(request):
	return render(request, 'tes2.html')

def regis(request):
	return render(request, 'regis.html')	

# Create your views here.
def jadwal(request):
	if request.method == 'POST':
		form = NameForm(request.POST or None)
		if form.is_valid():
			form.save()
	else:
		form = NameForm()
	context = {'form': form}

	return render(request, 'kegiatan.html', context)

def agenda(request):
	events = Jadwal.objects.all()
	return render(request, 'agenda.html', {'events': events})

def reset(request):
	events = Jadwal.objects.all().delete()
	return redirect("/Fadhil/agenda")