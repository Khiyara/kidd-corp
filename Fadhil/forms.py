from django import forms
from .models import Jadwal

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class NameForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = '__all__'
        widgets = {
            'date': DateInput(),
            'time': TimeInput()
        }

